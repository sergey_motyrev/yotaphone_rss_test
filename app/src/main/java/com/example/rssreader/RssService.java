package com.example.rssreader;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by MS on 28.04.2015.
 */
public class RssService extends IntentService {
    public static final String BROADCAST_HTTP_OK = "com.example.rssreader.BROADCAST_HTTP_OK";
    public static final String BROADCAST_HTTP_FAIL = "com.example.rssreader.BROADCAST_HTTP_FAIL";

    public static final String EXTRA_RESPONSE = "com.example.rssreader.EXTRA_RESPONSE";
    public static final String EXTRA_ERROR = "com.example.rssreader.EXTRA_ERROR";


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public RssService() {
        super(RssService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Uri uri = intent.getData();
        if (uri == null) {
            sendBroadcast(new Intent(BROADCAST_HTTP_FAIL)
                    .putExtra(EXTRA_ERROR, "Wrong url provided"));
            return;
        }

        HttpURLConnection http = null;
        try {
            http = (HttpURLConnection) new URL(uri.toString()).openConnection();
            http.setConnectTimeout(3000);
            http.setReadTimeout(7000);
            http.setDoInput(true);

            int responseCode = http.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                sendBroadcast(new Intent(BROADCAST_HTTP_FAIL)
                        .putExtra(EXTRA_ERROR, "Wrong response code: " + responseCode));
                return;
            }

            Map<String, List<String>> fields = http.getHeaderFields();
            for (String key : fields.keySet()) Log.i("head: "+key, fields.get(key).toString());

            // reading RSS or ATOM feed
            ArrayList<Entry> list = new ArrayList<>();
            InputStream in = http.getInputStream();
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in, null);
                parser.nextTag();
                boolean isRss = false, isAtom = false;
                switch (parser.getName()) {
                    case "rss":
                        isRss = true;
                        parser.require(XmlPullParser.START_TAG, null, "rss");
                        break;
                    case "feed":
                        isAtom = true;
                        parser.require(XmlPullParser.START_TAG, null, "feed");
                        break;
                    default:
                        throw new RuntimeException("not valid xml.");
                }

                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) continue;
                    String name = parser.getName();
                    if (isAtom && name.equals("entry")) {
                        list.add(Entry.readEntry(parser));
                    } else if (isRss && name.equals("channel")) {
                        parser.require(XmlPullParser.START_TAG, null, "channel");
                        while (parser.next() != XmlPullParser.END_TAG) {
                            if (parser.getEventType() != XmlPullParser.START_TAG) continue;
                            switch (parser.getName()) {
                                case "item": list.add(Entry.readItem(parser)); break;
                                default: skip(parser);
                            }
                        }
                    }  else {
                        skip(parser);
                    }
                }
            } finally {
                in.close();
            }

            Entry[] response = list.toArray(new Entry[list.size()]);
            sendBroadcast(new Intent(BROADCAST_HTTP_OK)
                    .putExtra(EXTRA_RESPONSE, response));
        } catch (IOException | XmlPullParserException | RuntimeException e) {
            e.printStackTrace();
            sendBroadcast(new Intent(BROADCAST_HTTP_FAIL)
                    .putExtra(EXTRA_ERROR, "Error: " + e));
        } finally {
            if (http != null) http.disconnect();
        }
    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    public static class Entry implements Parcelable {
        public final String title;
        public final String text;

        private Entry(String title, String text) {
            this.title = title;
            this.text = text;
        }
        public Entry(Parcel in){
            String[] data = new String[2];
            in.readStringArray(data);
            this.title = data[0];
            this.text = data[1];
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringArray(new String[] {title, text});
        }

        public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
            public Entry createFromParcel(Parcel in) {
                return new Entry(in);
            }

            public Entry[] newArray(int size) {
                return new Entry[size];
            }
        };

        public static Entry readEntry(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, null, "entry");
            String title = null, text = null;
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) continue;
                switch (parser.getName()) {
                    case "title": title = readText("title", parser); break;
                    case "summary": text = readText("summary", parser); break;
                    default: skip(parser);
                }
            }
            return new Entry(title, text);
        }

        public static Entry readItem(XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, null, "item");
            String title = null, text = null;
            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) continue;
                switch (parser.getName()) {
                    case "title": title = readText("title", parser); break;
                    case "description": text = readText("description", parser); break;
                    default: skip(parser);
                }
            }
//            Log.wtf(title, text);
            return new Entry(title, text);
        }

        private static String readText(String tag, XmlPullParser parser) throws IOException, XmlPullParserException {
            parser.require(XmlPullParser.START_TAG, null, tag);
            String result = "";
            if (parser.next() == XmlPullParser.TEXT) {
                result = parser.getText();
                parser.nextTag();
            }
            parser.require(XmlPullParser.END_TAG, null, tag);
            return result;
        }

        @Override
        public String toString() {
            return title + ": " +text;
        }
    }
}
