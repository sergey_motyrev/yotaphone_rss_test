package com.example.rssreader;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends Activity {
    BroadcastReceiver mBroadcastReceiver;
    IntentFilter mIntentFilter;
    EditText mEditRssUrl;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEditRssUrl = (EditText) findViewById(R.id.editRssUrl);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case RssService.BROADCAST_HTTP_FAIL:
                        String error = intent.getStringExtra(RssService.EXTRA_ERROR);
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                        break;
                    case RssService.BROADCAST_HTTP_OK:
                        Parcelable[] list = intent.getParcelableArrayExtra(RssService.EXTRA_RESPONSE);
                        Intent bsActivity = new Intent(context, BackScreenActivity.class)
                                .putExtra(BackScreenActivity.EXTRA_FEED_ITEMS, list);
                        startService(bsActivity);
                        break;
                }
                mProgressBar.setVisibility(View.GONE);
            }
        };
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(RssService.BROADCAST_HTTP_OK);
        mIntentFilter.addAction(RssService.BROADCAST_HTTP_FAIL);

        Spinner spinner = (Spinner) findViewById(R.id.urlPreset);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.feed_urls, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String url = parent.getItemAtPosition(position).toString();
                mEditRssUrl.setText(url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void loadRss(View v) {
        Uri uri = Uri.parse(mEditRssUrl.getText().toString());
        if (!uri.getScheme().matches("https?")) {
            Toast.makeText(getApplicationContext(), "Wrong url", Toast.LENGTH_SHORT).show();
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        startService(new Intent(this, RssService.class).setData(uri));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
    }
}
