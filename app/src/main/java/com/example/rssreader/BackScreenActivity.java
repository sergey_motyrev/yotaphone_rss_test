package com.example.rssreader;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yotadevices.sdk.BSActivity;
import com.yotadevices.sdk.BSDrawer;
import com.yotadevices.sdk.BSMotionEvent;

/**
 * Created by MS on 28.04.2015.
 */
public class BackScreenActivity extends BSActivity {
    public static final String EXTRA_FEED_ITEMS = "com.example.rssreader.EXTRA_FEED_ITEMS";

    LinearLayout mContainer;
    TextView mTitle;
    WebView mTextWebView;

    int position;
    Parcelable[] items;
    boolean drawFinished;

    void draw() {
        if (items == null || items.length == 0) {
            //feed is empty :(
            mTitle.setText("Feed is empty");
            getBSDrawer().drawBitmap(mTitle, BSDrawer.Waveform.WAVEFORM_GC_PARTIAL);
            return;
        }
        if (position >= items.length) position = 0;
        if (position < 0) position = items.length - 1;

        RssService.Entry entry = (RssService.Entry) items[position];
        mTitle.setText("#" + position + " " + entry.title);
        mTextWebView.loadData(entry.text, "text/html; charset=utf-8", null);
        mContainer.requestLayout();

        mContainer.post(new Runnable() {
            @Override
            public void run() {
                getBSDrawer().drawBitmap(mContainer, BSDrawer.Waveform.WAVEFORM_GC_PARTIAL);
            }
        });
    }

    @Override
    protected void onBSCreate() {
        super.onBSCreate();

        mTitle = new TextView(getContext());
        int title_padding = getContext().getResources().getDimensionPixelSize(R.dimen.title_padding);
        mTitle.setPadding(title_padding, title_padding, title_padding, title_padding);
        mTitle.setTextColor(Color.WHITE);
        mTitle.setBackgroundColor(Color.BLACK);
        mTitle.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.title_size));
        mTitle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mTextWebView = new WebView(getContext());
//        mTextWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        mTextWebView.getSettings().setDefaultTextEncodingName("UTF-8");
        mTextWebView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mContainer = new LinearLayout(getContext());
        mContainer.setOrientation(LinearLayout.VERTICAL);
        mContainer.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mContainer.addView(mTitle);
        mContainer.addView(mTextWebView);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        super.onHandleIntent(intent);
        if (intent.hasExtra(EXTRA_FEED_ITEMS)) {
            items = intent.getParcelableArrayExtra(EXTRA_FEED_ITEMS);
        }
        draw();
    }

    @Override
    protected void onBSSaveInstanceState(Bundle outState) {
        super.onBSSaveInstanceState(outState);
        outState.putInt("position", position);
    }

    @Override
    protected void onBSRestoreInstanceState(Bundle savedInstanceState) {
        super.onBSRestoreInstanceState(savedInstanceState);
        position = savedInstanceState.getInt("position", 0);
    }

    @Override
    protected void onBSTouchEvent(BSMotionEvent motionEvent) {
        super.onBSTouchEvent(motionEvent);
        switch (motionEvent.getBSAction()) {
            case GESTURES_BS_LR:
                position++;
                draw();
                break;
            case GESTURES_BS_RL:
                position--;
                draw();
                break;
            default:
                break;
        }
    }
}
